﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity
    {
        [MaxLength(12)]
        public string Name { get; set; }

        [MaxLength(12)]
        public string Description { get; set; }
    }
}