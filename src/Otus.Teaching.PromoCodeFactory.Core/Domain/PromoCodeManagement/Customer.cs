﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(12)]
        public string FirstName { get; set; }
        [MaxLength(12)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";
        [MaxLength(20)]
        public string Email { get; set; }

        //TODO: Списки Preferences и Promocodes 

        public virtual List<Preference> Preferences { get; set; }

        public virtual List<PromoCode> PromoCodes { get; set; }
    }
}