﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>: IRepository<T> where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task CreateAsync(T entity)
        {
            var newData = Data.ToList();
            newData.Add(entity);
            return Task.FromResult(newData);
        }

        public Task UpdateAsync(T entity)
        {
            var newData = Data.ToList();
            var index = newData.FindIndex(x => x == entity);
            newData[index] = entity;
            return Task.FromResult(newData);
        }

        public Task DeleteAsync(Guid id)
        {
            return Task.FromResult(Data.Where(x => x.Id != id));
        }
    }
}