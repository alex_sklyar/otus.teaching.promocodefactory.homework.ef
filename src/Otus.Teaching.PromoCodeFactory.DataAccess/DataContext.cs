﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Employee> Employess { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        public DataContext()
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>()
                .HasOne(x => x.Role)
                .WithOne()
                .HasForeignKey("RoleId");

            modelBuilder.Entity<Customer>()
                .HasMany(x => x.Preferences)
                .WithMany(x => x.Customers)
                .UsingEntity<Dictionary<string, object>>("CustomerPreferences",
                y => y.HasOne<Preference>()
                .WithMany()
                .HasForeignKey("PreferenceId"),
                y => y.HasOne<Customer>()
                .WithMany()
                .HasForeignKey("CustomerId"));

            modelBuilder.Entity<PromoCode>()
                .HasOne(x => x.Customer)
                .WithMany(x => x.PromoCodes)
                .HasForeignKey("CustomerId");

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            optionsBuilder.UseSqlite("Data Source = PromoCodeFactory.db");

        }

    }
}
